#!/bin/sh

export SENTRY_AUTH_TOKEN=<your-sentry-auth-token>
export SENTRY_ORG=<your-sentry-org>
export SENTRY_PROJECT=<your-sentry-project>

# export SENTRY_LOG_LEVEL=debug

shorebird flutter versions use 3.10.6

shorebird release ios-alpha --flavor development -- --obfuscate --split-debug-info=./build/app/outputs/bundle/release/symbols --dart-define="ENVIRONMENT=development" --dart-define="SENTRY_DSN=<your-sentry-dsn>"

# shorebird release android --flavor development -- --obfuscate --split-debug-info=./build/app/outputs/bundle/release/symbols --dart-define="ENVIRONMENT=development" --dart-define="SENTRY_DSN=<your-sentry-dsn>"


# flutter build ipa --release --obfuscate --split-debug-info=./build/app/outputs/symbols/ --export-method=development
# flutter build apk --obfuscate --split-debug-info=./build/app/outputs/symbols/
flutter packages pub run sentry_dart_plugin
