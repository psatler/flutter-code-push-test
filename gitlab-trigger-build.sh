#!/bin/bash
ANDROID=$1
IOS=$2

echo "ANDROID ${ANDROID}"
echo "IOS ${IOS}"
echo "BUILD_TYPE ${BUILD_TYPE}" # comes from gitlab ci panel UI and should be 'patch' or 'release'
echo

# Receives the workflow to run and also a string to indicate which OS it is triggering the build for
# the other variables comes from Gitlab env vars. More on https://gitlab.com/psatler/flutter-code-push-test/-/settings/ci_cd
_triggerBuild() {
    OS_BUILD=$1
    WORKFLOW=$2

    echo "Triggering a build for $OS_BUILD"
    curl --location 'https://api.codemagic.io/builds' \
    --header "x-auth-token: $CODE_MAGIC_AUTH_TOKEN" \
    --header 'Content-Type: application/json' \
    --data '{
        "appId": "'$CODE_MAGIC_APP_ID'",
        "workflowId": "'$WORKFLOW'",
        "branch": "'$CI_COMMIT_BRANCH'",
        "environment": {
            "variables": {
                "TYPE": "'$BUILD_TYPE'"
            }
        }
    }'
}


if [ $ANDROID == "true" ]
then
    if [ $BUILD_TYPE == "patch" ]
    then
        _triggerBuild "Android patch" $CODE_MAGIC_ANDROID_DEVELOPMENT_PATCH_WORKFLOW_ID
    elif [ $BUILD_TYPE == "release" ]
    then
        _triggerBuild "Android release" $CODE_MAGIC_ANDROID_DEVELOPMENT_RELEASE_WORKFLOW_ID
    fi
fi

if [ $IOS == "true" ]
then
    if [ $BUILD_TYPE == "patch" ]
    then
        _triggerBuild "iOS patch" "ios_development_patch" # ios_development_patch is the workflow id in the codemagic.yaml
    elif [ $BUILD_TYPE == "release" ]
    then
        _triggerBuild "iOS release" "ios_development_release"
    fi
fi

