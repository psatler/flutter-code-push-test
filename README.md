# codepush_test

A new Flutter project.

## Getting Started

This project is a starting point for a Flutter application.

A few resources to get you started if this is your first Flutter project:

- [Lab: Write your first Flutter app](https://docs.flutter.dev/get-started/codelab)
- [Cookbook: Useful Flutter samples](https://docs.flutter.dev/cookbook)

For help getting started with Flutter development, view the
[online documentation](https://docs.flutter.dev/), which offers tutorials,
samples, guidance on mobile development, and a full API reference.



## Flavors

- https://pub.dev/packages/flutter_launcher_icons#flavor-support

```
flutter pub run flutter_launcher_icons -f flutter_launcher_icons*
```

- then run `shorebird doctor` to check if everything is ok

## Shorebird

- [install shorebird CLI](https://docs.shorebird.dev/)

- [using different versions](https://docs.shorebird.dev/flutter-version#use-a-different-flutter-versions)

```bash
curl --proto '=https' --tlsv1.2 https://raw.githubusercontent.com/shorebirdtech/install/main/install.sh -sSf | bash
```

- https://docs.shorebird.dev/guides/flavors

```bash
shorebird init
```

- this project is currently using flutter `3.19.5`, therefore, 

```bash
shorebird flutter versions use 3.19.5
```

- creating a release

```bash
# Create a release for the development flavor
shorebird release android --flavor development

shorebird release ios-alpha --flavor development

# Create a release for the production flavor
shorebird release android --flavor production

shorebird release ios-alpha --flavor production
```

- creating a preview to run on device and/or emulators

```bash
# Preview the release for the development flavor.
shorebird preview --app-id 75d10f55-6f7e-470a-b74a-fca4c3ba316e --release-version 1.0.0+1

# Preview the release for the production flavor.
shorebird preview --app-id 7d1a3e22-b78d-451d-a91e-ff140d8efdf0 --release-version 1.0.0+1
```

- creating a patch first for development

```bash
shorebird patch android --flavor development

shorebird patch android --flavor development -- --obfuscate --split-debug-info=./build/app/outputs/bundle/release/symbols --dart-define="ENVIRONMENT=development"
```

- promoting patch to production

```bash
shorebird patch android --flavor production
```


### Codemagic

- https://docs.codemagic.io/yaml-quick-start/building-a-flutter-app/
- publishing android: https://docs.codemagic.io/yaml-publishing/google-play/#configure-google-play-api-access
- commonly used environment variables: https://docs.codemagic.io/yaml-basic-configuration/configuring-environment-variables/#commonly-used-variable-examples
- branching strategies (codemagic): https://docs.codemagic.io/knowledge-white-label/white-label-branching-strategies/
- build rest api: https://docs.codemagic.io/rest-api/builds/
## Misc.

- https://support.google.com/work/android/thread/136343688/can-i-upload-two-identical-same-applications-on-my-google-play-console-account?hl=en#:~:text=First%3A%20We%20suggest%20you%20don,use%20a%20unique%20package%20name.

- difference between google play tracks: https://support.google.com/googleplay/android-developer/answer/9845334?hl=en

- yaml Folded Block Scalar” `>`: https://docs.ansible.com/ansible/latest/reference_appendices/YAMLSyntax.html

- iOS error _Cannot save Signing Certificates without certificate private key_: https://docs.codemagic.io/partials/code-signing-ios-obtain-certificate/
    - add a variable called `CERTIFICATE_PRIVATE_KEY` a group and reference this group in _yaml_. The value of it can be obtained
    by running the command below on the already created _ios certificate_ (_code signinig certificate_) **NOTE**: we also need the password that was generated
    ```
    openssl pkcs12 -in IOS_DISTRIBUTION.p12 -nodes -nocerts | openssl rsa -out ios_distribution_private_key
    ```
    - more info also at https://blog.codemagic.io/app-store-connect-api-codemagic-cli-tools/ and at https://docs.codemagic.io/knowledge-others/import-variables-from-env-file/#add-environment-variables-to-codemagicyaml

- storing binaries: https://docs.codemagic.io/yaml-basic-configuration/configuring-environment-variables/#storing-binary-files

- [delete committed files](https://devconnected.com/how-to-delete-file-on-git)

- creating tokens for _sentry dart plugin_:
    - https://psatler.sentry.io/settings/auth-tokens/
    - to create a new custom integration https://sentry.io/settings/developer-settings/new-internal/
    - Env vars to use on codemagic
        - `SENTRY_PROJECT`
        - `SENTRY_ORG`
        - `SENTRY_AUTH_TOKEN`: token from the custom integration created
        - `SENTRY_DSN`: used in the `main.dart`

