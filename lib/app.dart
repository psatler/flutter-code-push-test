import 'package:codepush_test/sentry_errors.dart';
import 'package:flutter/material.dart';

class App extends StatelessWidget {
  const App({
    Key? key,
    required this.environment,
  }) : super(key: key);

  final String environment;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: const Text(
          'Code push test - Gitlab CI - Patchy',
          style: TextStyle(
            color: Colors.deepOrange,
          ),
        ),
        actions: [
          IconButton(
            onPressed: () {
              SentryErrorTestPage.route(context);
            },
            icon: const Icon(Icons.settings),
          ),
        ],
      ),
      body: Padding(
        padding: const EdgeInsets.all(16.0),
        child: Column(
          children: [
            const SizedBox(height: 40),
            Text('Environment: $environment'),
            const SizedBox(height: 40),
            ClipRRect(
              borderRadius: BorderRadius.circular(16),
              child: Image.network(
                'https://s1.static.brasilescola.uol.com.br/be/conteudo/images/big-ben.jpg',
              ),
            ),
            const SizedBox(height: 40),
            const Text('iOS Testflight code push'),
            const SizedBox(height: 8),
            const Text('iOS patch on 1.0.1+2'),
            const SizedBox(height: 8),
            Wrap(
              alignment: WrapAlignment.spaceBetween,
              runAlignment: WrapAlignment.spaceBetween,
              runSpacing: 8,
              spacing: 16,
              children: [
                ElevatedButton(
                  onPressed: () {},
                  child: Text('Left side button'),
                ),
                ElevatedButton(
                  onPressed: () {},
                  child: Text('Right side button'),
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}
