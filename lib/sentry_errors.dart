import 'package:flutter/material.dart';
import 'package:shorebird_code_push/shorebird_code_push.dart';

class SentryErrorTestPage extends StatefulWidget {
  const SentryErrorTestPage({super.key});

  static route(BuildContext context) {
    Navigator.of(context).push(
      MaterialPageRoute(
        builder: (context) => const SentryErrorTestPage(),
      ),
    );
  }

  @override
  State<SentryErrorTestPage> createState() => _SentryErrorTestPageState();
}

class _SentryErrorTestPageState extends State<SentryErrorTestPage> {
  // Create an instance of the ShorebirdCodePush class
  late final shorebirdCodePush = ShorebirdCodePush();

  Map<String, dynamic> _map = {};

  @override
  void initState() {
    super.initState();

    getVersion();
  }

  Future<void> getVersion() async {
    // Get the current patch version, or null if no patch is installed.
    final currentPatchversion = await shorebirdCodePush.currentPatchNumber();
    final nextPatchNumber = await shorebirdCodePush.nextPatchNumber();
    final isNewPatchAvailableForDownload =
        await shorebirdCodePush.isNewPatchAvailableForDownload();

    final isNewPatchReadyToInstall =
        await shorebirdCodePush.isNewPatchReadyToInstall();
    final isShorebirdAvailable = shorebirdCodePush.isShorebirdAvailable();

    if (!mounted) return;

    setState(() {
      _map = {
        'currentPatchversion': currentPatchversion,
        'nextPatchNumber': nextPatchNumber,
        'isNewPatchAvailableForDownload': isNewPatchAvailableForDownload,
        'isNewPatchReadyToInstall': isNewPatchReadyToInstall,
        'isShorebirdAvailable': isShorebirdAvailable,
      };
    });

    // Download a new patch.
    // await shorebirdCodePush.downloadUpdateIfAvailable();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text('Sentry Error Test Page')),
      body: Column(
        children: [
          ElevatedButton(
            onPressed: () {
              throw Exception('Sentry error test');
            },
            child: Text('Force error'),
          ),
          const SizedBox(height: 40),
          Text(_map['currentPatchversion']?.toString() ?? ''),
          Text(_map['nextPatchNumber']?.toString() ?? ''),
          Text(_map['isNewPatchAvailableForDownload']?.toString() ?? ''),
          Text(_map['isNewPatchReadyToInstall']?.toString() ?? ''),
          Text(_map['isShorebirdAvailable']?.toString() ?? ''),
        ],
      ),
    );
  }
}
