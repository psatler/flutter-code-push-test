class Environment {
  const Environment();

  String get environment => const String.fromEnvironment(
        'ENVIRONMENT',
        defaultValue: 'dev',
      );

  String get sentryDsn => const String.fromEnvironment(
        'SENTRY_DSN',
        defaultValue: '',
      );
}
