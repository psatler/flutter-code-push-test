import 'package:flutter/material.dart';

import 'package:codepush_test/environment.dart';
import 'package:sentry_flutter/sentry_flutter.dart';

import 'app.dart';

Future<void> main() async {
  final environment = const Environment().environment;

  await SentryFlutter.init(
    (options) {
      options.dsn = const Environment().sentryDsn;
      // Set tracesSampleRate to 1.0 to capture 100% of transactions for performance monitoring.
      // We recommend adjusting this value in production.
      options.tracesSampleRate = 1.0;
    },
    appRunner: () => runApp(MyApp(
      environment: environment,
    )),
  );
}

class MyApp extends StatelessWidget {
  const MyApp({
    Key? key,
    required this.environment,
  }) : super(key: key);

  final String environment;

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: environment != 'production',
      title: 'Flutter Demo',
      theme: ThemeData(
        colorScheme: ColorScheme.fromSeed(seedColor: Colors.orange),
        useMaterial3: true,
      ),
      home: App(
        environment: environment,
      ),
    );
  }
}
